from diagrams import Edge, Diagram
from diagrams.aws.compute import EC2
from diagrams.aws.general import InternetGateway
from diagrams.aws.network import VPC, PublicSubnet, RouteTable, VPCElasticNetworkInterface

with Diagram("Clustered Web Services", show=False):
    instance = EC2("Instance")
    ig = InternetGateway("InternetGateway")
    rt = RouteTable("RouteTable")
    subnet = PublicSubnet("Subnet")
    vpc = VPC("VPC")
    eni = VPCElasticNetworkInterface("NetworkInterface")

    instance >> eni >> subnet >> Edge(style="bold") << rt >> Edge(style="bold") << ig
    subnet >> vpc
    ig >> vpc